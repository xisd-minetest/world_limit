local modname = minetest.get_current_modname()
--~ local modpath = minetest.get_modpath(modname)


local pos_min = minetest.setting_get_pos(modname.."_min") or {
		x=-1024,
		y=-2048,
		z=-1024,
}
local pos_max = minetest.setting_get_pos(modname.."_max") or {
		x=1024,
		y=2048,
		z=1024,
}

local timer = 0
local timer_timeout = 2
minetest.register_globalstep(function(dtime)
	-- Update timer
	timer = timer + dtime
	if (timer >= timer_timeout) then
		
		-- List all connected player
		local players = minetest.get_connected_players()
		for _,player in ipairs(players) do

			local name = player:get_player_name()
			if not minetest.check_player_privs(name, {server = true}) then

				-- Get player pos
				local pos = player:getpos()
				
				local new_pos = { x=pos.x, y=pos.y, z=pos.z}
				if pos.x > pos_max.x then new_pos.x = ( pos_max.x - 16 ) end
				if pos.y > pos_max.y then new_pos.y = ( pos_max.y - 16 ) end
				if pos.z > pos_max.z then new_pos.z = ( pos_max.z - 16 ) end
				if pos.x < pos_min.x then new_pos.x = ( pos_min.x + 16 ) end
				if pos.y < pos_min.y then new_pos.y = ( pos_min.y + 16 ) end
				if pos.z < pos_min.z then new_pos.z = ( pos_min.z + 16 ) end
				
				if not ( new_pos.x == pos.x and new_pos.y == pos.y and new_pos.z == pos.z ) then
					print("pos is not new pos")
					minetest.chat_send_player(name, "Désolé, cette partie du monde n'est pas accessible pour le moment")
					if pos.y < new_pos.y then
						for i=pos.y,( pos_min.y + 2 ) do 
							minetest.set_node({ x=new_pos.x, y=i, z=new_pos.z}, {name="default:stone"})
						end
					end
					player:setpos(new_pos) 
				end
				
			end
		end
		-- Restart timer
		timer = 0
	end
end)
